# Terminal Palette Test Page

This is a tool to help visualize and test the contrast of terminal palettes with various common terminal applications that make use of colors.

[Open in Browser](https://kepstin.pages.gitlab.gnome.org/terminal-palette/)

Please contribute additional terminal applications for testing!

When you're developing this locally, not that you need to serve the application with a web server in order for the Javascript to load correctly.
You can run a simple web server with a one-liner python command:

```
python3 -m http.server --bind 127.0.0.1 --directory public 8000
```

And then open [http://localhost:8000](http://localhost:8000) in your web browser.
