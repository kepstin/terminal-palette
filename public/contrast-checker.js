/*
Copyright 2020 Calvin Walton <calvin.walton@kepstin.ca>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import * as wcagContrast from "./tmcw-wcag-contrast.js";

const fg = document.getElementById("contrast-checker-fg");
const bg = document.getElementById("contrast-checker-bg");
const wcagDisplay = document.getElementById("wcag-result");
wcagDisplay.style.minWidth = '6em';

function parseCSSColor(str) {
	let m;
	if (m = str.match(/^rgba?\(([0-9]+), ([0-9]+), ([0-9]+)/)) {
		return [ m[1], m[2], m[3] ];
	}
	if (m = str.match(/^#([0-9a-fA-F]{6})$/)) {
		const num = parseInt(m[1], 16);
		return [ num >> 16, (num >> 8) & 255, num & 255];
	}
}

function formatHexColor(rgb) {
	const num = (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
	const str = `00000${num.toString(16)}`.slice(-6);
	const colorStr = `#${str}`;
	return colorStr;
}

function updateResult(fgColor, bgColor) {
	const fgRGB = parseCSSColor(fgColor);
	const bgRGB = parseCSSColor(bgColor);

	fg.value = `${formatHexColor(fgRGB)}`;
	bg.value = `${formatHexColor(bgRGB)}`;

	const wcagResult = wcagContrast.rgb(fgRGB, bgRGB);
	const wcagScore = wcagContrast.score(wcagResult);
	wcagDisplay.textContent = `${wcagResult.toPrecision(3)} (${wcagScore})`;
}

function terminalClickListener(ev) {
	const element = ev.target;
	const style = getComputedStyle(element);

	updateResult(style.color, style.backgroundColor);
}

function colorInputListener(ev) {
	updateResult(fg.value, bg.value);
}

fg.addEventListener("input", colorInputListener);
bg.addEventListener("input", colorInputListener);
// Force recalculation on load to handle browser preserved input values
setTimeout(() => { fg.dispatchEvent(new InputEvent("input")); }, 0);

for (let terminal of document.querySelectorAll(".terminal")) {
	terminal.addEventListener("click", terminalClickListener);
}
